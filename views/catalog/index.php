<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Каталог</h2>
                    <div class="panel-group category-products">
                        <?php foreach ($categories as $categoryItem): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="/category/<?php echo $categoryItem['id'];?>">
                                            <?php echo $categoryItem['name'];?>
                                        </a>
                                    </h4>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div class="col-sm-9 padding-right">
                <div class="features_items">
                    <h2 class="title text-center">Останні товари</h2>

                    <div class="pull-left"></div>
                    <div class="pull-right">
                        <form class="search" method="post" action="">
                            <input type="search" name="search" placeholder="Пошук">
                            <button type="submit" class="btn btn-default btn-search"><i class="fa fa-search"></i> Шукати</button>
                        </form>
                    </div><br><br>

                    <?php foreach ($latestProducts as $product): ?>
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <?php if($product['availability']==1): ?>
                                            <a href="/product/<?php echo $product['id'];?>"><img height="240px" src="<?php echo Product::getImage($product['id']); ?>" alt="" /></a>
                                        <?php else: ?>
                                            <a href="/product/<?php echo $product['id'];?>"><img height="240px" src="<?php echo Product::getImage($product['id']); ?>" alt="" class="not-active"/></a>
                                        <?php endif; ?>
                                        <h2><?php echo $product['price'];?>₴</h2>
                                        <p>
                                            <a href="/product/<?php echo $product['id'];?>">
                                                <?php echo $product['name'];?>
                                            </a>
                                        </p>
                                        <?php if($product['availability']==1): ?>
                                            <a href="#" class="btn btn-default add-to-cart" data-id="<?php echo $product['id']; ?>"><i class="fa fa-shopping-cart"></i>В кошик</a>
                                        <?php else: ?>
                                            <p class="btn btn-default not-active" data-id="<?php echo $product['id']; ?>"><i class="fa fa-shopping-cart"></i> Немає</p>
                                        <?php endif; ?>
                                    </div>
                                    <?php if ($product['is_new']): ?>
                                        <img src="/template/images/home/new.png" class="new" alt="" />
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>                   

                </div>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>