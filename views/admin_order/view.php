<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Адмінпанель</a></li>
                    <li><a href="/admin/order">Керування замовленнями</a></li>
                    <li class="active">Перегляд замовлення</li>
                </ol>
            </div>
            <div class="admin-form">

                <h2>Перегляд замовлення №<?php echo $order['id']; ?></h2>
                <a href="/admin/order/" class="btn btn-default back"><i class="fa fa-arrow-left"></i>  Назад</a>

                <h4>Інформація про замовлення</h4>
                <table class="table-admin-small table-bordered table">
                    <tr>
                        <th>Номер замовлення</th>
                        <td><?php echo $order['id']; ?></td>
                    </tr>
                    <tr>
                        <th>Ім'я покупця</th>
                        <td><?php echo $order['user_name']; ?></td>
                    </tr>
                    <tr>
                        <th>Телефон покупця</th>
                        <td><?php echo $order['user_phone']; ?></td>
                    </tr>
                    <tr>
                        <th>Коментарій покупця</th>
                        <td><?php echo $order['user_comment']; ?></td>
                    </tr>
                    <?php if ($order['user_id'] != 0): ?>
                        <tr>
                            <th>ID покупця</th>
                            <td><?php echo $order['user_id']; ?></td>
                        </tr>
                    <?php endif; ?>
                    <tr>
                        <th>Статус замовлення</th>
                        <td><?php echo Order::getStatusText($order['status']); ?></td>
                    </tr>
                    <tr>
                        <th>Дата замовлення</th>
                        <td><?php echo $order['date']; ?></td>
                    </tr>
                </table>

                <br>
                <h4>Товари в замовленні</h4>

                <table class="table-admin-medium table-bordered table ">
                    <tr>
                        <th>ID товару</th>
                        <th>Артикул товару</th>
                        <th>Назва</th>
                        <th>Ціна</th>
                        <th>Кількість</th>
                    </tr>
                    <?php foreach ($products as $product): ?>
                        <tr>
                            <td><?php echo $product['id']; ?></td>
                            <td><?php echo $product['code']; ?></td>
                            <td><?php echo $product['name']; ?></td>
                            <td>$<?php echo $product['price']; ?></td>
                            <td><?php echo $productsQuantity[$product['id']]; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>

            </div>
        </div>

</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

