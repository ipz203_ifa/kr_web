<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Адмінпанель</a></li>
                    <li><a href="/admin/order">Керування замовленнями</a></li>
                    <li class="active">Редагувати замовлення</li>
                </ol>
            </div>

            <h2>Редагувати замовлення №<?php echo $id; ?></h2>
            <br/>

            <div class="admin-form">
                <form action="#" method="post">

                    <p>Ім'я покупця</p>
                    <input type="text" name="userName" placeholder="" value="<?php echo $order['user_name']; ?>"><br><br>

                    <p>Телефон покупця</p>
                    <input type="text" name="userPhone" placeholder="" value="<?php echo $order['user_phone']; ?>"><br><br>

                    <p>Коментарій покупця</p>
                    <input class="editor" type="text" name="userComment" placeholder="" value="<?php echo $order['user_comment']; ?>"><br>

                    <p>Дата оформлення замовлення</p>
                    <input type="text" name="date" placeholder="" value="<?php echo $order['date']; ?>"><br><br>

                    <p>Статус</p>
                    <select name="status">
                        <option value="1" <?php if ($order['status'] == 1) echo ' selected="selected"'; ?>>Нове
                            замовлення
                        </option>
                        <option value="2" <?php if ($order['status'] == 2) echo ' selected="selected"'; ?>>В обробці
                        </option>
                        <option value="3" <?php if ($order['status'] == 3) echo ' selected="selected"'; ?>>
                            Доставляєтбся
                        </option>
                        <option value="4" <?php if ($order['status'] == 4) echo ' selected="selected"'; ?>>Закрито
                        </option>
                    </select>
                    <br>
                    <br>
                    <button type="submit" name="submit" class="btn btn-default">Зберегти</button><br><br>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

