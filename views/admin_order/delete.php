<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Адмінпанель</a></li>
                    <li><a href="/admin/order">Керування замовленнями</a></li>
                    <li class="active">Видалити замовлення</li>
                </ol>
            </div>

            <h2>Видалити замовлення №<?php echo $id; ?></h2><br>
            <p>Ви дійсно хочете видалити це замовлення?</p><br>

            <div class="admin-form">
                <form method="post">
                    <button type="submit" name="submit">Видалити</button>
                </form>
            </div>

        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

