<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Адмінпанель</a></li>
                    <li><a href="/admin/category">Керування категоріями</a></li>
                    <li class="active">Редагувати категорію</li>
                </ol>
            </div>

            <h2>Редагувати категорію "<?php echo $category['name']; ?>"</h2>
            <br/>

            <div class="admin-form">
                <form action="#" method="post">
                    <p>Назва</p>
                    <input class="editor" type="text" name="name" placeholder="" value="<?php echo $category['name']; ?>"><br>

                    <p>Порядковий номер</p>
                    <input type="text" name="sort_order" placeholder="" value="<?php echo $category['sort_order']; ?>"><br><br>

                    <p>Статус</p>
                    <select name="status">
                        <option value="1" <?php if ($category['status'] == 1) echo ' selected="selected"'; ?>>
                            Відображається
                        </option>
                        <option value="0" <?php if ($category['status'] == 0) echo ' selected="selected"'; ?>>
                            Прихована
                        </option>
                    </select>

                    <br><br>

                    <button type="submit" name="submit" class="btn btn-default">Зберегти</button>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

