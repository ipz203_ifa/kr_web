<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Адмінпанель</a></li>
                    <li><a href="/admin/category">Керування категоріями</a></li>
                    <li class="active">Видалити категорію</li>
                </ol>
            </div>

            <h2>Видалити категорію №<?php echo $id; ?></h2><br>
            <p>Ви дійсно хочете видалити цю категорію?</p><br>

            <div class="admin-form">
                <form method="post">
                    <button type="submit" name="submit">Видалити</button>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

