<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Адмінпанель</a></li>
                    <li><a href="/admin/order">Керування категоріями</a></li>
                    <li class="active">Додати категорію</li>
                </ol>
            </div>

            <h2>Додати нову категорію</h2>
            <br/>

            <?php if (isset($errors) && is_array($errors)): ?>
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <li class="error"> <?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

            <div class="admin-form">
                <form action="#" method="post">
                    <p>Назва</p>
                    <input class="editor" type="text" name="name" placeholder="" value=""><br>

                    <p>Порядковий номер</p>
                    <input type="text" name="sort_order" placeholder="" value=""><br><br>

                    <p>Статус</p>
                    <select name="status">
                        <option value="1" selected="selected">Відображається</option>
                        <option value="0">Прихована</option>
                    </select>

                    <br><br>

                    <button type="submit" name="submit" class="btn btn-default">Зберегти</button>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

