<?php include ROOT . '/views/layouts/header.php'; ?>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Каталог</h2>
                        <div class="panel-group category-products">
                            <?php foreach ($categories as $categoryItem): ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a href="/category/<?php echo $categoryItem['id']; ?>">
                                                <?php echo $categoryItem['name']; ?>
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>

                <div class="col-sm-9 padding-right">
                    <div class="features_items">
                        <h2 class="title text-center">Кошик</h2>


                        <?php if ($result): ?>
                            <p id="success">Замовлення успішно оформлено</p>
                        <?php else: ?>

                            <p id="total_checkout">Обрано товарів: <b><?php echo $totalQuantity; ?></b><br>Сума:
                                <b><?php echo $totalPrice; ?>₴</b></p><br/>

                            <?php if (!$result): ?>

                                <div class="col-sm-4">
                                    <?php if (isset($errors) && is_array($errors)): ?>
                                        <ul>
                                            <?php foreach ($errors as $error): ?>
                                                <li class="error"><?php echo $error; ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                    <div id="checkout_form">
                                        <p>Будь ласка заповніть форму для оформлення замовлення.</p>

                                        <div class="login-form">
                                            <form action="#" method="post">

                                                <p>Ім'я:</p>
                                                <input type="text" required name="userName" placeholder="Ім'я"
                                                       value="<?php echo $userName; ?>"/>

                                                <p>Номер телефону:</p>
                                                <input type="tel" required name="userPhone" placeholder="Телефон"
                                                       value="<?php echo $userPhone; ?>"/>

                                                <p>Комментарій до замовлення:</p>
                                                <input type="text" name="userComment" placeholder="Коментарій"
                                                       value="<?php echo $userComment; ?>"/>

                                                <button type="submit" name="submit" class="btn btn-default">Замовити
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                            <?php endif; ?>

                        <?php endif; ?>

                    </div>

                </div>
            </div>
        </div>
    </section><br><br>

<?php include ROOT . '/views/layouts/footer.php'; ?>