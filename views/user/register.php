<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <div class="col-sm-4 col-sm-offset-4 padding-right">
                
                <?php if ($result): ?>
                    <p id="success">Вас успішно зареєстровано!</p>
                <?php else: ?>
                    <?php if (isset($errors) && is_array($errors)): ?>
                        <ul>
                            <?php foreach ($errors as $error): ?>
                                <li class="error"><?php echo $error; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>

                    <div class="signup-form"><!--sign up form-->
                        <h2>Реєстрація на сайті</h2>
                        <form action="#" method="post">
                            <input type="text" required name="name" placeholder="Ім'я" value="<?php echo $name; ?>"/>
                            <input type="email" required name="email" placeholder="E-mail" value="<?php echo $email; ?>"/>
                            <input type="password" required name="password" placeholder="Пароль" value="<?php echo $password; ?>"/>
                            <button type="submit" name="submit" class="btn btn-default">Зареєструватися</button>
                        </form>
                    </div><!--/sign up form-->
                
                <?php endif; ?>
                <br/>
                <br/>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>