<?php include ROOT . '/views/layouts/header.php'; ?>

    <section>
        <div class="container">
            <div class="row">

                <div class="col-sm-4 col-sm-offset-4 padding-right">

                    <?php if ($result): ?>
                        <p id="success">Ви успішно залишили відгук. Дякуємо!</p>
                    <?php else: ?>
                        <?php if (isset($errors) && is_array($errors)): ?>
                            <ul>
                                <?php foreach ($errors as $error): ?>
                                    <li class="error"> <?php echo $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>

                        <div class="signup-form"><!--sign up form-->
                            <h2>Додати відгук</h2>
                            <form action="#" method="post">
                                <p>Ваше ім'я:</p>
                                <input type="text" required name="user_name" placeholder="Ім'я" value="<?php echo $name; ?>"/>

                                <p>Відгук:</p>
                                <textarea required id="add_review" name="text" placeholder="Відгук"></textarea>
                                <br/>
                                <button type="submit" name="submit" class="btn btn-default">Додати</button>
                            </form>
                        </div>

                    <?php endif; ?>
                    <br/>
                    <br/>
                </div>
            </div>
        </div>
    </section>

<?php include ROOT . '/views/layouts/footer.php'; ?>