<?php include ROOT . '/views/layouts/header.php'; ?>

    <section>
        <div class="container">
            <table class="row">
                <h2>Історія покупок</h2><br>
                <table class="table-bordered table">
                    <tr>
                        <th>ID</th>
                        <th>Ім'я</th>
                        <th>Телефон</th>
                        <th>Дата</th>
                        <th colspan="2">Товари</th>
                        <th>Статус</th>
                        <th>Коментар</th>
                    </tr>
                    <?php foreach ($ordersList as $order): ?>
                        <tr>
                            <td><?php echo $order['id']; ?></td>
                            <td><?php echo $order['user_name']; ?></td>
                            <td><?php echo $order['user_phone']; ?></td>
                            <td><?php echo $order['date']; ?></td>
                            <td>
                                <?php foreach (Cabinet::getProductsInHistory($order['products'], 1) as $product): ?>
                                    <a href="/product/<?php echo $product['id']; ?>"><?php echo $product['name']; ?><br></a>
                                <?php endforeach; ?>
                            </td>
                            <td>
                                <?php foreach (Cabinet::getProductsInHistory($order['products'], 0) as $product): ?>
                                    <?php echo $product; ?><br>
                                <?php endforeach; ?>
                            </td>
                            <td><?php echo $order['status']; ?></td>
                            <td><?php echo $order['user_comment']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
                <br/>
                <br/>
            </div>
        </div>
    </section>

<?php include ROOT . '/views/layouts/footer.php'; ?>