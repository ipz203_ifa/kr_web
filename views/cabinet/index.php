<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row cabinet">
            <h2 align="center">Вітаю, <?php echo $user['name'];?>!</h2><br>
            <div id="cabinet_options">
                <ul>
                    <li><a href="/cabinet/edit">Редагувати дані</a></li><br>
                    <li><a href="/cabinet/history">Історія покупок</a></li>
                    <li><a href="/cabinet/add">Залишити відгук</a></li>
                </ul>
            </div>
            
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>