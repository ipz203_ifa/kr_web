<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Адмінпанель</a></li>
                    <li><a href="/admin/order">Керування замовленнями</a></li>
                    <li class="active">Редагувати дані користувача</li>
                </ol>
            </div>

            <h2>Редагувати користувача №<?php echo $id; ?></h2>
            <br/>

            <div class="admin-form">
                <form action="#" method="post">

                    <p>Ім'я користувача</p>
                    <input required type="text" name="userName" placeholder="" value="<?php echo $user['name']; ?>"><br><br>

                    <p>E-mail</p>
                    <input required type="email" name="userEmail" placeholder="" value="<?php echo $user['email']; ?>"><br><br>

                    <p>Роль</p>
                    <input class="editor" type="text" name="userRole" placeholder="" value="<?php echo $user['role']; ?>"><br>
                    <br>
                    <br>
                    <button type="submit" name="submit" class="btn btn-default">Зберегти</button><br><br>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

