<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Адмінпанель</a></li>
                    <li class="active">Керування користувачами</li>
                </ol>
            </div>
            <div class="admin-form">

                <h2>Список користувачів</h2><br/>

                <table class="table-bordered table">
                    <tr>
                        <th>ID користувача</th>
                        <th>Ім'я користувача</th>
                        <th>E-mail</th>
                        <th>Роль</th>
                        <th></th>
                        <th></th>
                    </tr>
                    <?php foreach ($usersList as $user): ?>
                        <tr>
                            <td><?php echo $user['id']; ?></td>
                            <td><?php echo $user['name']; ?></td>
                            <td><?php echo $user['email']; ?></td>
                            <td><?php echo $user['role']; ?></td>
                            <td><a href="/admin/users/update/<?php echo $user['id']; ?>" title="Редагувати"><i
                                        class="fa fa-pencil-square-o"></i></a></td>
                            <td><a href="/admin/users/delete/<?php echo $user['id']; ?>" title="Видалити"><i
                                        class="fa fa-times"></i></a></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

