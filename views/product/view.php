<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Каталог</h2>
                    <div class="panel-group category-products">
                        <?php foreach ($categories as $categoryItem): ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="/category/<?php echo $categoryItem['id']; ?>">
                                            <?php echo $categoryItem['name']; ?>
                                        </a>
                                    </h4>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>

            <div class="col-sm-9 padding-right">
                <div class="product-details"><!--product-details-->
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="view-product">
                                <?php if($product['availability']==1): ?>
                                    <img src="<?php echo Product::getImage($product['id']); ?>" alt="" data-fancybox="gallery"/>
                                <?php else: ?>
                                    <img src="<?php echo Product::getImage($product['id']); ?>" alt="" class="not-active" data-fancybox="gallery"/>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="product-information"><!--/product-information-->

                                <?php if ($product['is_new']): ?>
                                    <img src="/template/images/product-details/new.jpg" class="newarrival" alt="" />
                                <?php endif; ?>

                                <h2><?php echo $product['name']; ?></h2>
                                <p>Код товару: <?php echo $product['code']; ?></p>
                                <span>
                                    <span><?php echo $product['price']; ?>₴</span>
                                    <?php if($product['availability']==1): ?>
                                        <a href="#" class="btn btn-default add-to-cart" data-id="<?php echo $product['id']; ?>"><i class="fa fa-shopping-cart"></i>В кошик</a>
                                    <?php else: ?>
                                        <p class="btn btn-default not-active" data-id="<?php echo $product['id']; ?>"><i class="fa fa-shopping-cart"></i> Немає</p>
                                    <?php endif; ?>
                                </span>
                                <p><b>Наявність:</b> <?php echo Product::getAvailabilityText($product['availability']); ?></p>
                                <p><b>Виробник:</b> <?php echo $product['brand']; ?></p>
                            </div><!--/product-information-->
                        </div>
                    </div>
                    <div class="row">                                
                        <div class="col-sm-12">
                            <br/>
                            <h5>Опис товару</h5>
                            <?php echo $product['description']; ?>
                        </div>
                    </div>
                </div><!--/product-details-->

            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>