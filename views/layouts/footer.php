<div class="page-buffer"></div>
</div>

<footer id="footer" class="page-footer"><!--Footer-->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">SvitSmachnogo © 2024</p>
                <p class="social-icons pull-right">
                    <a href="#"><i class="fa fa-instagram"></i></a>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-youtube"></i></a>
                </p>
            </div>
        </div>
    </div>
</footer><!--/Footer-->


<script src="/template/js/jquery.js"></script>
<script src="/template/js/jquery.cycle2.min.js"></script>
<script src="/template/js/jquery.cycle2.carousel.min.js"></script>
<script src="/template/js/bootstrap.min.js"></script>
<script src="/template/js/jquery.scrollUp.min.js"></script>
<script src="/template/js/price-range.js"></script>
<script src="/template/js/jquery.prettyPhoto.js"></script>
<script src="/template/js/main.js"></script>

<!--Асинхронний запит-->
<script>
    $(document).ready(function () { //після завантаження документа
        $(".add-to-cart").click(function () { //при натискані на елемент з класом add-to-cart
            var id = $(this).attr("data-id"); //зчитуємо на якому саме елементі подія (id)
            $.post("/cart/addAjax/" + id, {}, function (data) { // "/cart/addAjax/" + id - адрес обробника запита, date - id товару
                $("#cart-count").html(data); //записуємо в cart-count результат
            });
            return false;
        });
    });
</script>

</body>
</html>