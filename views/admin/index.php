<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <br/>
            <div class="admin-main">
                <h2>Добрий день, <?php echo User::getUserById($_SESSION['user'])['name']; ?>!</h2><br>
                <h4>Ви ввішли в режим адміністрування сайтом.</h4>
                <br/><br>
                <ul>
                    <a href="/admin/product"><li><i class="fa fa-barcode"></i> Керувати товарами</li></a>
                    <a href="/admin/category"><li><i class="fa fa-folder-open"></i> Керувати категоріями</li></a>
                    <a href="/admin/order"><li><i class="fa fa-truck"></i> Керувати замовленнями</li></a>
                    <a href="/admin/users"><li><i class="fa fa-users"></i> Керувати користувачами</li></a>
                </ul>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

