<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Адмінпанель</a></li>
                    <li><a href="/admin/product">Керування товарами</a></li>
                    <li class="active">Видалити товар</li>
                </ol>
            </div>

            <h2>Видалити товар №<?php echo $id; ?></h2><br>
            <p>Ви дійсно бажаєте видалити цей товар?</p><br>

            <div class="admin-form">
                <form method="post">
                    <button type="submit" name="submit" class="btn btn-default">Видалити</button>
                </form>
            </div>

        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

