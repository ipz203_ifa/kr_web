<?php include ROOT . '/views/layouts/header_admin.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <br/>

            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/admin">Адмінпанель</a></li>
                    <li><a href="/admin/product">Керування товарами</a></li>
                    <li class="active">Додати товар</li>
                </ol>
            </div>


            <h2>Додати новий товар</h2>
            <br/>

            <?php if (isset($errors) && is_array($errors)): ?>
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <li class="error"> <?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

            <div class="admin-form">
                <form action="#" method="post" enctype="multipart/form-data">

                    <p>Назва товара</p>
                    <input class="editor" type="text" name="name" placeholder="" value=""><br/>

                    <p>Артикул</p>
                    <input type="text" name="code" placeholder="" value=""><br/><br/>

                    <p>Вартість, $</p>
                    <input class="editor" type="text" name="price" placeholder="" value=""><br/>

                    <p>Категорія</p>
                    <select name="category_id">
                        <?php if (is_array($categoriesList)): ?>
                            <?php foreach ($categoriesList as $category): ?>
                                <option value="<?php echo $category['id']; ?>">
                                    <?php echo $category['name']; ?>
                                </option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>

                    <br/><br/>

                    <p>Виробник</p>
                    <input type="text" name="brand" placeholder="" value=""><br/><br/>

                    <p>Зображення товара</p>
                    <input type="file" name="image" placeholder="" value=""><br/>

                    <p>Детальний опис</p>
                    <textarea class="editor" name="description"></textarea>

                    <br/>

                    <p>Наявність на складі</p>
                    <select name="availability">
                        <option value="1" selected="selected">Так</option>
                        <option value="0">Ні</option>
                    </select>

                    <br/><br/>

                    <p>Новинка</p>
                    <select name="is_new">
                        <option value="1" selected="selected">Так</option>
                        <option value="0">Ні</option>
                    </select>

                    <br/><br/>

                    <p>Рекомендовані</p>
                    <select name="is_recommended">
                        <option value="1" selected="selected">Так</option>
                        <option value="0">Ні</option>
                    </select>

                    <br/><br/>

                    <p>Статус</p>
                    <select name="status">
                        <option value="1" selected="selected">Відображається</option>
                        <option value="0">Прихований</option>
                    </select>

                    <br/><br/>

                    <button type="submit" name="submit" class="btn btn-default">Зберегти</button>

                    <br/><br/>

                </form>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer_admin.php'; ?>

