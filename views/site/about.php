<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <div class="col-lg-6">
                <h2>Інформація про магазин</h2>

                <br/>

                <p>Ласкаво просимо до SvitSmachnogo – вашого вірного провідника у світі ароматних та смачних горішків! У нашому онлайн-магазині ви знайдете найвибірніші сорти горішків з усього світу,
                    які стануть справжнім джерелом задоволення для вашого піднебіння та енергії для вашого дня. Наша місія – дарувати вам якісні продукти з найкращих джерел,
                    щоб ви могли насолоджуватися кожним кустиночком. Виберіть свій шлях у світ смаку з SvitSmachnogo! </p>
            </div>
            <div id="about_photo"><img src="/template/images/home/about.jpg"></div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>