<?php include ROOT . '/views/layouts/header.php'; ?>
    <div class="header-bottom">
        <img height="275px" src="/template/images/home/reviews.jpeg" alt=""/>
    </div>

    <section>
        <div class="container">
            <div>
                <div class="item">
                    <?php foreach ($listReviews as $review): ?>
                        <div class="product-image-wrapper review_block">
                            <h3><?php echo $review['user_name']; ?></h3>
                            <small><?php echo $review['date']; ?></small><br><br>
                            <p><?php echo $review['text']; ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div><br>
            <!-- Постраничная навигация -->
            <?php echo $pagination->get(); ?>
        </div>
    </section>
    <br><br>
<?php include ROOT . '/views/layouts/footer.php'; ?>