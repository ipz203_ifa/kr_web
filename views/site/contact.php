<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <div class="col-sm-4 col-sm-offset-4 padding-right">

                <?php if ($result): ?>
                    <p id="success">Повідомлення успішно відправлено!</p>
                <?php else: ?>
                    <?php if (isset($errors) && is_array($errors)): ?>
                        <ul>
                            <?php foreach ($errors as $error): ?>
                                <li> - <?php echo $error; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>

                    <div class="signup-form"><!--sign up form-->
                        <h2>Залиште своє питання</h2>
                        <form action="#" method="post">
                            <p>Ваша пошта</p>
                            <input type="email" required name="userEmail" placeholder="E-mail" value="<?php echo $userEmail; ?>"/>
                            <p>Повідомлення</p>
                            <input type="text" required name="userText" placeholder="Ваше повідомлення" value="<?php echo $userText; ?>"/>
                            <br/>
                            <button type="submit" name="submit" class="btn btn-default">Надіслати</form>
                        </form>
                    </div><!--/sign up form-->
                <?php endif; ?>
                <br/>
                <br/>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>