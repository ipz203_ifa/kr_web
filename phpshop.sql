-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 02, 2024 at 12:46 AM
-- Server version: 8.0.30
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int NOT NULL COMMENT 'id категорії',
  `name` varchar(255) NOT NULL COMMENT 'Назва категорії',
  `sort_order` int NOT NULL DEFAULT '0' COMMENT 'Порядковий номер',
  `status` int NOT NULL DEFAULT '1' COMMENT 'Статус (1 - відображати, 0 - приховати)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `sort_order`, `status`) VALUES
(1, 'Сухофрукти', 1, 1),
(2, 'Горіхи', 2, 1),
(3, 'Спеції', 3, 1),
(4, 'Мед', 4, 1),
(5, 'Бакалія', 5, 1),
(6, 'Олія', 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int NOT NULL COMMENT 'id товара',
  `name` varchar(255) NOT NULL COMMENT 'Назва товара',
  `category_id` int NOT NULL COMMENT 'id категорії',
  `code` int NOT NULL COMMENT 'Артикул товара',
  `price` float NOT NULL COMMENT 'Ціна товара',
  `availability` int NOT NULL COMMENT 'Наявність товара',
  `brand` varchar(255) NOT NULL COMMENT 'Виробник товара',
  `description` text NOT NULL COMMENT 'Детальний опис',
  `is_new` int NOT NULL DEFAULT '0' COMMENT 'Новинка',
  `is_recommended` int NOT NULL DEFAULT '0' COMMENT 'Рекомендувати',
  `status` int NOT NULL DEFAULT '1' COMMENT 'Статус (1 - відображати, 0 - приховати)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `category_id`, `code`, `price`, `availability`, `brand`, `description`, `is_new`, `is_recommended`, `status`) VALUES
(1, 'Курага «Джамбо»', 1, 110551, 599, 1, 'Україна', '<p>Солодка курага Джамбо від SvitSmachnogo - натуральна смакова експлозія, яка подарує вам миті насолоди. Спробуйте цей величезний смак у найкращому вигляді<br>', 1, 0, 1),
(2, 'Фініки натуральні «Мазафаті» вищий сорт', 1, 110552, 340, 1, 'Турція', 'На шляху до здорового харчування - фініки натуральні «Мазафаті» вищого сорту від SvitSmachnogo. Смакуйте елітний вибір природної солодощі та корисності з кожним кусочком!', 0, 0, 1),
(3, 'Журавлина сушена', 1, 110553, 439, 1, 'Польща', 'Насолоджуйтесь соковитою і смачною журавлиною сушеною від SvitSmachnogo. Цей натуральний десерт прекрасно підійде як перекус вдома чи в дорозі. Поспішайте замовити свою порцію свіжості прямо зараз!', 0, 1, 1),
(4, 'Чорнослив в’ялений', 1, 121771, 359, 0, 'Україна', 'Відчуйте смак натурального енергетичного заряду з в’яленого чорносливу від SvitSmachnogo. Насичений смак і корисні властивості зроблять його ідеальним перекусом у будь-який час дня. Замовте прямо зараз та насолоджуйтесь свіжістю природи!', 0, 0, 1),
(5, 'Чорнослив копчений', 1, 121772, 359, 0, 'Україна', 'Додайте новий вимір смаку до вашого перекусу з чорносливом копченим від SvitSmachnogo. Цей витончений десерт поєднує в собі неперевершений смак і аромат копчення, роблячи його ідеальним вибором для гурманів. Замовте зараз і насолоджуйтесь справжнім задоволенням!', 0, 0, 1),
(6, 'Волоський горіх «Половинки»', 2, 121773, 200, 0, 'Україна', 'Проведіть час із задоволенням, смакуючи волоські горішки \'Половинки\' від SvitSmachnogo. Натуральний смак і корисність кожного горішка створять незабутні враження. Почуйте насолоду в кожному кустику - замовте прямо зараз', 0, 0, 1),
(7, 'Горіх макадамія', 2, 115141, 599, 1, 'Македонія', 'Поглибіться у світ розкоші з горіхами макадамія від SvitSmachnogo. Ці неперевершені джеми смаку пропонують неповторний баланс багатства смаку і корисних властивостей. Замовте прямо зараз і насолоджуйтеся кожним кустиночком елітного смаку', 0, 0, 1),
(8, 'Кеш’ю несмажений', 2, 110581, 299, 1, 'Турція\r\n', 'Зануртеся у світ натурального смаку з несмаженими кеш\'ю від SvitSmachnogo. Ці горішки приголомшують своїм відтінком і корисними властивостями. Збагачуйте своє харчування здоровими перекусами - замовте зараз', 1, 0, 1),
(9, 'Кеш’ю смажений', 2, 134032, 290, 0, 'Україна\r\n', 'Смажений кеш\'ю від SvitSmachnogo - смак, який завойовує серця! Насолоджуйтесь хрусткою текстурою та неперевершеним ароматом кожного горішка. Відкрийте для себе новий рівень смаку - замовте прямо зараз!', 1, 1, 1),
(10, 'Горіхи Alesto', 2, 3007676, 1949, 1, 'Alesto', 'Відчуйте чарівний смак з Alesto! Найвища якість горіхів, оброблених з увагою до кожної деталі. Замовте свою порцію насолоди прямо зараз', 0, 0, 1),
(11, 'Суміш 10 овочей', 3, 3004047, 100, 1, 'Україна', 'Поглибіться у різноманіття смаків та корисних властивостей з нашою сумішшю з 10 овочів. Вибір вітамінів та смаків у кожному кустику. Спробуйте зараз для смакового вибору та здорового харчування', 0, 0, 1),
(12, 'Приправи для ухи', 3, 30233, 100, 0, 'Україна', 'Додайте аромату та смаку до вашої ухи з нашою колекцією найкращих приправ. Незалежно від того, чи ви віддаєте перевагу традиційним смакам або шукаєте щось нове, у нас є все для вашого ідеального смаку. Поглибіться у світ смаку з нашими приправами для ухи прямо зараз', 0, 0, 1),
(13, 'Перець чорний мелений', 3, 134025, 100, 1, 'Україна', 'Насолоджуйтеся неповторним смаком та ароматом з нашим чорним перцем меленим. Кожна порція вибухає смаком, надаючи вашій страві чарівну пікантність. Додайте вишуканість вашим стравам з нашим чорним перцем меленим від SvitSmachnogo', 0, 0, 1),
(14, 'Чіа біла', 5, 12010, 199, 1, 'Польща', 'Відкрийте для себе безмежні можливості з чіа білої від SvitSmachnogo. Цей натуральний суперфуд відомий своїми корисними властивостями та унікальним смаком. Додайте їх до своєї щоденної дієти для підтримки здоров\'я та відчуйте енергію в кожній крихті. Замовте свою порцію чіа білої вже сьогодні', 1, 1, 1),
(15, 'Мед  манука', 4, 11763, 499, 1, 'Нова Зеландія', 'Поглибіться у світ корисностей з нашим медом манука. Відомий своїми унікальними лікувальними властивостями, цей мед пропонує натуральну підтримку вашого здоров\'я. Солодкий смак і багатий аромат зроблять ваші страви неперевершеними. Поспішайте замовити мед манука від SvitSmachnogo і відчуйте його благотворний вплив на ваше здоров\'я вже сьогодні', 0, 0, 1),
(16, 'Олія соняшникова', 6, 717026, 89, 0, 'Hobby World', 'Насолоджуйтеся найчистішою якістю з нашою олією соняшниковою. Збагачена вітамінами та незамінними жирними кислотами, ця олія прекрасно підходить для приготування різноманітних страв. Додайте смаку та корисності своїй кулінарній майстерності з олією соняшниковою від SvitSmachnogo', 0, 0, 1),
(18, 'Сочевиця Україна', 5, 485178, 149, 1, 'Україна', 'Подорожуйте у світ здорового харчування з нашою сочевицею Україна від SvitSmachnogo. Вона не лише насичена білком та важливими мікроелементами, але й має чудовий смак, що робить її відмінним доповненням до будь-якої страви. Додайте українську сочевицю до свого раціону для смачного та здорового харчування', 0, 0, 1),
(19, 'Сочевиця Туреччина', 5, 270259, 349, 1, 'Турція ', 'Поглибіться в ароматі та смаку з нашою сочевицею Туреччина від SvitSmachnogo. Вона відома своїм багатим смаком та високим вмістом білка, що робить її ідеальним доповненням до будь-якої страви. Додайте різноманіття та корисність вашому столу з сочевицею Туреччина від SvitSmachnogo', 0, 0, 1),
(20, 'Кіноа червона', 5, 643756, 300, 1, 'Турція', 'Додайте колориту та смаку до своїх страв з червоною кіноа від SvitSmachnogo. Ця неперевершена зернова культура, багата на білок та мікроелементи, додасть смакового розмаїття та корисності до будь-якого вашого обіду чи вечері. Замовте червону кіноа вже сьогодні та насолоджуйтесь її унікальним смаком', 0, 0, 1),
(21, 'Кіноа біла', 5, 909029, 300, 1, 'Турція', 'Дивіться на кулінарію з нової перспективи з нашою білою кіноа від SvitSmachnogo. Це чудове джерело білка та важливих мікроелементів, яке надасть вашим стравам особливий смак та корисні властивості. Додайте білу кіноа до свого раціону для різноманітного та здорового харчування', 1, 1, 1),
(22, 'Квасоля червона', 5, 566780, 79, 1, 'Україна', 'Поглибіться у світ смаку з червоною квасолею від SvitSmachnogo. Ця багата на білок та важливі поживні речовини культура стане чудовим доповненням до вашого раціону. Додайте її до салатів, супів або гарнірів для насолоди смаком та користі для здоров\'я. Замовте свою порцію червоної квасолі вже сьогодні', 0, 0, 1),
(23, 'Заправка для борщу', 3, 3004077, 100, 1, 'Україна', 'Створіть справжній шедевр української кухні з нашою заправкою для борщу від SvitSmachnogo. Ця унікальна суміш натуральних спецій та трав додасть вашому борщу неповторний смак та аромат. Збагатьте свій стіл традиційним українським смаком з нашою заправкою для борщу', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_order`
--

CREATE TABLE `product_order` (
  `id` int NOT NULL COMMENT 'id замовлення',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Ім''я покупця',
  `user_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Телефон покупця',
  `user_comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Коментар покупця',
  `user_id` int DEFAULT NULL COMMENT 'id покупця',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Дата замовлення',
  `products` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Список продуктів та їх к-ть',
  `status` int NOT NULL DEFAULT '1' COMMENT 'Статус замовлення (1 - Нове замовлення, 2 - В обробці, 3 - Доставляється, 4 - Закрито)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_order`
--

INSERT INTO `product_order` (`id`, `user_name`, `user_phone`, `user_comment`, `user_id`, `date`, `products`, `status`) VALUES
(2, 'Frants Ivanytskyi', '+380542312534', 'Хутчіше', 1, '2024-04-26 11:19:57', '{\"36\":2,\"37\":1}', 3),
(8, 'Frants', '+380646386125', 'Все круто', NULL, '2024-04-30 20:24:14', '{\"40\":1}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int NOT NULL COMMENT 'id відгука',
  `user_name` varchar(255) NOT NULL COMMENT 'Ім''я користувача',
  `user_id` int NOT NULL COMMENT 'id користувача',
  `text` text NOT NULL COMMENT 'Текст відгука',
  `date` date NOT NULL COMMENT 'Дата відгука'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `user_name`, `user_id`, `text`, `date`) VALUES
(2, 'Frants Ivanytskyi', 1, 'Шановна команда SvitSmachnogo,\r\n\r\nХочемо виразити щиру подяку за ваші надзвичайно смачні горішки! Кожен кустиночок виявився справжньою насолодою для наших смакових рецепторів. Ваша продукція вразила нас якістю та свіжістю, а обслуговування в магазині залишило лише приємні враження.\r\n\r\nМи вдячні за вашу працелюбність і прагнення забезпечувати найвищий рівень задоволення своїх клієнтів. Будемо рекомендувати ваш магазин усім нашим друзям і з нетерпінням очікуємо на наступну можливість насолодитися вашими продуктами.', '2024-01-05'),
(4, 'Дмитренко Юрій', 4, 'Хочу відзначити вашу вражаючу продукцію – справжню скарбницю смаку! Ваші горішки принесли мені незабутні відчуття та смакові експлозії. Я вражений вашою увагою до деталей і вишуканістю кожного кустиночка.\r\n\r\nДякую вам за вашу працьовитість і турботу про своїх клієнтів. Ви справжні майстри своєї справи і створюєте неперевершені смакові враження для нас, вашої вдячної аудиторії.', '2024-04-26'),
(6, 'Дмитренко Юрій', 4, 'Хочу відзначити вашу вражаючу продукцію – справжню скарбницю смаку! Ваші горішки принесли мені незабутні відчуття та смакові експлозії. Я вражений вашою увагою до деталей і вишуканістю кожного кустиночка.\r\n\r\nДякую вам за вашу працьовитість і турботу про своїх клієнтів. Ви справжні майстри своєї справи і створюєте неперевершені смакові враження для нас, вашої вдячної аудиторії.', '2024-04-26'),
(7, 'Frants Ivanytskyi', 1, 'Шановна команда SvitSmachnogo,\r\n\r\nХочемо виразити щиру подяку за ваші надзвичайно смачні горішки! Кожен кустиночок виявився справжньою насолодою для наших смакових рецепторів. Ваша продукція вразила нас якістю та свіжістю, а обслуговування в магазині залишило лише приємні враження.\r\n\r\nМи вдячні за вашу працелюбність і прагнення забезпечувати найвищий рівень задоволення своїх клієнтів. Будемо рекомендувати ваш магазин усім нашим друзям і з нетерпінням очікуємо на наступну можливість насолодитися вашими продуктами.', '2024-01-05');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int NOT NULL COMMENT 'id користувача',
  `name` varchar(255) NOT NULL COMMENT 'Ім''я користувача',
  `email` varchar(255) NOT NULL COMMENT 'Пошта користувача',
  `password` varchar(255) NOT NULL COMMENT 'Пароль користувача',
  `role` varchar(50) NOT NULL COMMENT 'Рівень доступу'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `role`) VALUES
(1, 'Frants Ivanytskyi', 'franciwanicki@gmail.com', '$2y$10$5d29EAKoTGkIO7aXtbu5Oe5hsQWJ', ''),
(2, 'Франц Іваницький', 'ipz203_ifa@student.ztu.edu.ua', '$2y$10$YuQoifoiSZVqNEvPAIouo.1AJjl', 'admin'),
(4, 'Дмитренко Юрій ', 'dmytrenko@gmail.com', '$2y$10$RoazBTyTA22CoxzmrorxB0eUKJUk8zWMe2AbuhVeic9goAu3uwoHNRoazBTyTA22', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id категорії', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id товара', AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id замовлення', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id відгука', AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id користувача', AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_order`
--
ALTER TABLE `product_order`
  ADD CONSTRAINT `product_order_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
