<?php

/**
 * Класс Db
 * Компонент для роботи з базою даних
 */
class Db
{

    /**
     * Встановлює з'єднання з базою даних
     * @return \PDO <p>Об'єкт класу PDO для роботи с БД</p>
     */
    public static function getConnection()
    {
        // Отримуємо параметри підключення з файла
        $paramsPath = ROOT . '/config/db_params.php';
        $params = include($paramsPath);

        // Встановлюємо з'єднання
        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        $db = new PDO($dsn, $params['user'], $params['password']);

        // Задаємо кодування
        $db->exec("set names utf8");

        return $db;
    }

}
