<?php

/**
 * Клас Cart
 * Компонент для роботи з коризною
 */
class Cart
{

    /**
     * Додання товара в кошик (сесію)
     * @param int $id <p>id товару</p>
     * @return integer <p>Кількість товарів в кошику</p>
     */
    public static function addProduct($id)
    {
        // Приводимо $id до типу integer
        $id = intval($id);

        // Пустий масив для товарів в кошику
        $productsInCart = array();

        // Якщо в кошику вже є товари (вони зберігаються в сесії)
        if (isset($_SESSION['products'])) {
            // То записуємо в наш масив товари
            $productsInCart = $_SESSION['products'];
        }

        // Перевіряємо чи є вже такий товар в кошику
        if (array_key_exists($id, $productsInCart)) {
            // Якщо такий товар є в кошику, але був додан ще раз, к-ть +1
            $productsInCart[$id] ++;
        } else {
            // Якщо ні, додаємо id нового товара в кошик з к-тю = 1
            $productsInCart[$id] = 1;
        }

        // Записуємо масив з товарами в сесію
        $_SESSION['products'] = $productsInCart;

        // Повертаємо к-ть товарів в кошику
        return self::countItems();
    }

    /**
     * Підрахунок кількості товарів в кошику (в сесії)
     * @return int <p>К-ть товарів в кошику</p>
     */
    public static function countItems()
    {
        // Перевірка наявності товарів в кошику
        if (isset($_SESSION['products'])) {
            // Якщо масив з товарами є
            // Підраховуємо і повертаємо їх к-ть
            $count = 0;
            foreach ($_SESSION['products'] as $id => $quantity) {
                $count = $count + $quantity;
            }
            return $count;
        } else {
            // Якщо товарів нема, повертаємо 0
            return 0;
        }
    }

    /**
     * Повертає масив з ідентифікаторами і к-тю товарів в кошику
     * Якщо товарів нема, повертає false;
     * @return mixed: boolean or array
     */
    public static function getProducts()
    {
        if (isset($_SESSION['products'])) {
            return $_SESSION['products'];
        }
        return false;
    }

    /**
     * Отримуємо загальну ціну переданих товарів общую
     * @param array $products <p>Масив з інформацією про товари</p>
     * @return integer <p>Загальна вартість</p>
     */
    public static function getTotalPrice($products)
    {
        // Отримуємо масив з ідентифікаторами і кількістю товарів в кошику
        $productsInCart = self::getProducts();

        // Підраховуємо загальну вартість
        $total = 0;
        if ($productsInCart) {
            // Якщо кошик непорожній
            // Проходимо по переданому в метод масиву товарів
            foreach ($products as $item) {
                // Рахуємо загальну вартість: ціна товара * к-ть товара
                $total += $item['price'] * $productsInCart[$item['id']];
            }
        }

        return $total;
    }

    /**
     * Очищує кошик
     */
    public static function clear()
    {
        if (isset($_SESSION['products'])) {
            unset($_SESSION['products']);
        }
    }

    /**
     * Видаляє товар з вказанним id з кошику
     * @param integer $id <p>id товара</p>
     */
    public static function deleteProduct($id)
    {
        // Отримуємо масив з ідентифікаторами і к-тю товарів в кошику
        $productsInCart = self::getProducts();

        // Видаляємо з масиву елемент з вказанним id
        unset($productsInCart[$id]);

        // Записуємо масив товарів з видаленим елементом в сесію
        $_SESSION['products'] = $productsInCart;
    }

}
