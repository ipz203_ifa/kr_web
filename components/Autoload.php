<?php

/**
 * Функція __autoload для автоматичного підключення класів
 */
function __autoload($class_name)
{
    // Масив папок, в яких можуть зберігатися необхідні класи
    $array_paths = array(
        '/models/',
        '/components/',
        '/controllers/',
    );

    // Проходимо по масиву папок
    foreach ($array_paths as $path) {

        // Формуємо ім'я та шлях до файла з класом
        $path = ROOT . $path . $class_name . '.php';

        // Якщо такий файл існує, підключаємо його
        if (is_file($path)) {
            include_once $path;
        }
    }
}
