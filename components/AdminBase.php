<?php

/**
 * Абстрактний клас AdminBase містить загальну логіку для контролерів,
 * що використовуются в панелі адміністратора
 */
abstract class AdminBase
{

    /**
     * Метод, який перевіряє користувача на те, чи є він адміністратором
     * @return boolean
     */
    public static function checkAdmin()
    {
        // Перевіряємо чи авторизований користувач. Якщо так - переадрисовуємо в панель, ні - виводимо повідомлення
        $userId = User::checkLogged();

        // Отримуємо інформацію про поточного користувача
        $user = User::getUserById($userId);

        // Якщо його роль "admin", пускаємо в адмінпанель
        if ($user['role'] == 'admin') {
            return true;
        }

        // Інакше завершуємо роботу з повідомленням про відмову доступу
        die("<div style='background-color: #db9797; border: 2px indianred solid; padding: 20px; '>Відмовлено.<br> Ви не маєте необхідного рівня доступу.</div>");
    }

}
