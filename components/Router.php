<?php

/**
 * Класс Router
 * Компонент для роботи з маршрутами
 */
class Router
{

    /**
     * Властивість для зберігання масива шляхів
     * @var array 
     */
    private $routes;

    /**
     * Конструктор
     */
    public function __construct()
    {
        // Шлях до файла з роутами
        $routesPath = ROOT . '/config/routes.php';

        // Отримуємо роути з файла
        $this->routes = include($routesPath);
    }

    /**
     * Повертає рядок запита
     */
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    /**
     * Метод для обробки запита
     */
    public function run()
    {
        // Отримуємо рядок запита
        $uri = $this->getURI();

        // Перевіряємо наявність такого запита в масиві маршрутів (routes.php)
        foreach ($this->routes as $uriPattern => $path) {

            // Порівнюємо $uriPattern і $uri
            if (preg_match("~$uriPattern~", $uri)) {

                // Отримуємо внутрішній шлях з зовнішнього відповідно до правила.
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                // Визначаємо контролер, action, параметри

                $segments = explode('/', $internalRoute);

                $controllerName = array_shift($segments) . 'Controller';
                $controllerName = ucfirst($controllerName);

                $actionName = 'action' . ucfirst(array_shift($segments));

                $parameters = $segments;

                // Підключаємо файл класа-контролера
                $controllerFile = ROOT . '/controllers/' .
                        $controllerName . '.php';

                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                }

                // Створити об'єкт, визвати метод (т.е. action)
                $controllerObject = new $controllerName;

                /* Викликаємо необхідний метод ($actionName) у певного
                 * класа ($controllerObject) з заданими ($parameters) параметрами
                 */
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);

                // Якщо метод контролера успішно викликаний, завершуємо роботу роутера
                if ($result != null) {
                    break;
                }
            }
        }
    }

}
