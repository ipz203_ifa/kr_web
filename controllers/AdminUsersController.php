<?php

/**
 * Контролер AdminUsersController
 * Управління замовленнями в адмінпанелі
 */
class AdminUsersController extends AdminBase
{

    /**
     * Action для сторінки "Керування користувачами"
     */
    public function actionIndex()
    {
        // Перевірка доступу
        self::checkAdmin();

        // Отримуємо список користувачів
        $usersList = User::getUsersList();

        // Підключаємо вид
        require_once(ROOT . '/views/admin_users/index.php');
        return true;
    }

    /**
     * Action для сторінки "Редагування користувача"
     */
    public function actionUpdate($id)
    {
        // Перевірка доступу
        self::checkAdmin();

        // Отримуємо дані про певного користувача
        $user = User::getUserById($id);

        // Обробка форми
        if (isset($_POST['submit'])) {
            // Якщо форма відправлена
            // Отримуємо дані з форми
            $userName = $_POST['userName'];
            $userEmail = $_POST['userEmail'];
            $userRole = $_POST['userRole'];

            // Зберігаємо зміни
            User::updateUserById($id, $userName, $userEmail, $userRole);

            // Перенаправляємо користувача на сторінку керування користувачами
            header("Location: /admin/users");
        }

        // Підключаємо вид
        require_once(ROOT . '/views/admin_users/update.php');
        return true;
    }

    /**
     * Action для сторінки "Видалити користувача"
     */
    public function actionDelete($id)
    {
        // Перевірка доступу
        self::checkAdmin();

        // Обробка форми
        if (isset($_POST['submit'])) {
            // Якщо форма відправлена
            // Видалняємо замовлення
            User::deleteUserById($id);

            // Перенаправляємо користувача на сторінку керування користувачами
            header("Location: /admin/users");
        }

        // Підключаємо вид
        require_once(ROOT . '/views/admin_users/delete.php');
        return true;
    }

}
