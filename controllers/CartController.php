<?php

/**
 * Контролер CartController
 * Кошик
 */
class CartController
{

    /**
     * Action для додання товара в кошик за допомогою асинхронного запита (ajax)
     * @param integer $id <p>id товара</p>
     */
    public function actionAddAjax($id)
    {
        // Додаємо товар в кошик і друкуємо результат: к-ть товарів в кошику
        echo Cart::addProduct($id);
        return true;
    }
    
    /**
     * Action для видалення товара з коршика
     * @param integer $id <p>id товара</p>
     */
    public function actionDelete($id)
    {
        // Видаляємо заданий товар з кошика
        Cart::deleteProduct($id);

        // Повертаємо користувача в кошик
        header("Location: /cart");
    }

    /**
     * Action для сторінки "Кошик"
     */
    public function actionIndex()
    {
        // Список категорій для лівого меню
        $categories = Category::getCategoriesList();

        // Отримуємо ідентифікатори і к-ть товарів в кошику
        $productsInCart = Cart::getProducts();

        if ($productsInCart) {
            // Якщо в кошику є товари, отримуємо повну інформацію про товари для списка
            // Отримуємо масив тільки з ідентифікаторами товарів
            $productsIds = array_keys($productsInCart);

            // Отримуємо масив з повною інформаціємо про необхідні товари
            $products = Product::getProdustsByIds($productsIds);

            // Отримуємо загальну вартість товарів
            $totalPrice = Cart::getTotalPrice($products);
        }

        // Підключаємо вид
        require_once(ROOT . '/views/cart/index.php');
        return true;
    }

    /**
     * Action для сторінки "Оформлення покупки"
     */
    public function actionCheckout()
    {
        // Отримуємо данні з кошика   и
        $productsInCart = Cart::getProducts();

        // Якщо товарів нема, вцідправляємо користувача шукати товари на головну
        if ($productsInCart == false) {
            header("Location: /");
        }

        // Список категорій для лівого меню
        $categories = Category::getCategoriesList();

        // Знаходимо загальну вартість
        $productsIds = array_keys($productsInCart);
        $products = Product::getProdustsByIds($productsIds);
        $totalPrice = Cart::getTotalPrice($products);

        // К-ть товарів
        $totalQuantity = Cart::countItems();

        // Поля для форми
        $userName = false;
        $userPhone = false;
        $userComment = false;

        // Статус успішного оформлення замовлення
        $result = false;

        // Перевіряємо чи є користувач гостем
        if (!User::isGuest()) {
            // Якщо користувач не гість
            // Отримуємо інформацію про користувача з БД
            $userId = User::checkLogged();
            $user = User::getUserById($userId);
            $userName = $user['name'];
        } else {
            // Якщо гість, поля форми залишуться порожними
            $userId = null;
        }

        // Обробка форми
        if (isset($_POST['submit'])) {
            // Якщо форма відправлена
            // Отримуємо дані з форми
            $userName = $_POST['userName'];
            $userPhone = $_POST['userPhone'];
            $userComment = $_POST['userComment'];

            // Прапорець помилок
            $errors = false;

            // Валідація полів
            if (!User::checkName($userName)) {
                $errors[] = "Невірне ім'я";
            }
            if (!User::checkPhone($userPhone)) {
                $errors[] = 'Невірний телефон';
            }


            if ($errors == false) {
                // Якщо помилок немає
                // Зберігаємо замовлення в базі даних
                $result = Order::save($userName, $userPhone, $userComment, $userId, $productsInCart);

                if ($result) {
                    // Якщо замовлення успішно збережено
                    // Сповіщаємо адміністратора про нове замовлення
                    $adminEmail = 'kn201_bas@student.ztu.edu.ua';
                    $message = '<a href="witcher-shop/admin/orders">Список замовлень</a>';
                    $subject = 'Нове замовлення!';
                    mail($adminEmail, $subject, $message);

                    // Очищаємо коршик
                    Cart::clear();
                }
            }
        }

        // Підключаємо вид
        require_once(ROOT . '/views/cart/checkout.php');
        return true;
    }

}
