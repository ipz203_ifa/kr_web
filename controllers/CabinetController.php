<?php

/**
 * Контролер CabinetController
 * Кабінет користувача
 */
class CabinetController
{

    /**
     * Action для сторінки "Кабінет користувача"
     */
    public function actionIndex()
    {
        // Отримуємо ідентифікатор користувача з сесії
        $userId = User::checkLogged();

        // Отримуємо інформацію про користувача з БД
        $user = User::getUserById($userId);

        // Підключаємо вид
        require_once(ROOT . '/views/cabinet/index.php');
        return true;
    }

    /**
     * Action для сторінки "Історія покупок"
     */
    public function actionHistory()
    {
        // Отримуємо ідентифікатор користувача з сесії
        $userId = User::checkLogged();

        // Отримуємо список замовлень даного користувача
        $ordersList = Cabinet::getOrdersList($userId);

        // Підключаємо вид
        require_once(ROOT . '/views/cabinet/history.php');
        return true;
    }

    /**
     * Action для сторінки "Редагування даних користувача"
     */
    public function actionEdit()
    {
        // Отримуємо ідентифікатор користувача з сесії
        $userId = User::checkLogged();

        // Отримуємо інформацію про користувача з БД
        $user = User::getUserById($userId);

        // Заповняємо змінні для полів форми
        $name = $user['name'];
        $password = $user['password'];

        // Прапорець результату
        $result = false;

        // Обробка форми
        if (isset($_POST['submit'])) {
            // Якщо форма відправлена
            // Отримуємо дані з форми редагування
            $name = $_POST['name'];
            $password = $_POST['password'];

            // Прапорець помилок
            $errors = false;

            // Валідуємо значення
            if (!User::checkName($name)) {
                $errors[] = "Ім'я не може бути менше 2-х символів";
            }
            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не може бути менше 6-ти символів';
            }

            if ($errors == false) {
                // Якщо помилок немає, зберігаємо зміни профілю
                $result = User::edit($userId, $name, $password);
            }
        }

        // Підключаємо вид
        require_once(ROOT . '/views/cabinet/edit.php');
        return true;
    }

    /**
     * Action для сторінки "Залишити відгук користувача"
     */
    public function actionAdd()
    {
        // Отримуємо ідентифікатор користувача з сесії
        $userId = User::checkLogged();

        // Отримуємо інформацію про користувача з БД
        $user = User::getUserById($userId);

        // Заповняємо змінні для полів форми
        $name = $user['name'];

        // Прапорець результату
        $result = false;

        // Обробка форми
        if (isset($_POST['submit'])) {
            // Якщо форма відправлена
            // Отримуємо дані з форми
            $user_name = $_POST['user_name'];
            $text = $_POST['text'];
            $date = date("Y-m-d");

            // Прапорець помилок
            $errors = false;

            // Валидируем значения
            if (!User::checkName($name)) {
                $errors[] = "Ім'я не може бути менше 2-х символів";
            }
            if ($errors == false) {
                // Якщо помилок немає, додаємо новий відгук
                $result = User::add($user_name, $text, $date);
            }
        }

        // Підключаємо вид
        require_once(ROOT . '/views/cabinet/add.php');
        return true;
    }

}
