<?php

/**
 * Контролер CatalogController
 * Каталог товарів
 */
class CatalogController
{

    /**
     * Action для сторінки "Каталог товарів"
     */
    public function actionIndex()
    {
        // Список категорій для лівого меню
        $categories = Category::getCategoriesList();

        if (isset($_POST['search'])) {
            //Список шуканих товарів
            $latestProducts = Product::getSearchProducts(12, $_POST['search']);
        }
        else{
            // Список останніх товарів
            $latestProducts = Product::getLatestProducts(12);
        }

        // Підключаємо вид
        require_once(ROOT . '/views/catalog/index.php');
        return true;
    }

    /**
     * Action для сторінки "Категорія товарів"
     */
    public function actionCategory($categoryId, $page = 1)
    {
        // Список категорій для лівого меню
        $categories = Category::getCategoriesList();

        if($_POST['availability'] == 1) {
            $categoryProducts = Product::getProductsListByCategory($categoryId, $page, [1], $_POST['sort'], $_POST['order']);
            // Створюємо об'єкт Pagination - посторінкова навігація
            $pagination = new Pagination(Product::getProductsListByCategory($categoryId, $page, [0, 1], $_POST['sort'], $_POST['order'], 1), $page, Product::SHOW_BY_DEFAULT, 'page-');
        }
        else {
            $categoryProducts = Product::getProductsListByCategory($categoryId, $page, [0, 1], $_POST['sort'], $_POST['order']);
            // Загальна к-ть товарів (для посторінкової навігації)
            $total = Product::getTotalProductsInCategory($categoryId);
            // Створюємо об'єкт Pagination - посторінкова навігація
            $pagination = new Pagination($total, $page, Product::SHOW_BY_DEFAULT, 'page-');
        }

        // Підключаємо вид
        require_once(ROOT . '/views/catalog/category.php');
        return true;
    }

}
