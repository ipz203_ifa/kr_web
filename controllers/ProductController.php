<?php

/**
 * Контролер ProductController
 * Товар
 */
class ProductController
{

    /**
     * Action для сторінки перегляда товару
     * @param integer $productId <p>id товара</p>
     */
    public function actionView($productId)
    {
        // Список категорій для лівого меню
        $categories = Category::getCategoriesList();

        // Отримуємо інформацію про товар
        $product = Product::getProductById($productId);

        // Підключаємо вид
        require_once(ROOT . '/views/product/view.php');
        return true;
    }

}
