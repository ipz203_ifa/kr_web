<?php

/**
 * Контролер AdminController
 * Головна сторінка в адмінпанелі
 */
class AdminController extends AdminBase
{
    /**
     * Action для початкової сторінки "Панель адміністратора"
     */
    public function actionIndex()
    {
        // Перевірка доступа
        self::checkAdmin();

        // Підключаємо вид
        require_once(ROOT . '/views/admin/index.php');
        return true;
    }

}
