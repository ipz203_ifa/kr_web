<?php

/**
 * Контролер AdminCategoryController
 * Керування категоріями товарів в адмінпанелі
 */
class AdminCategoryController extends AdminBase
{

    /**
     * Action для сторінки "Керування категоріями"
     */
    public function actionIndex()
    {
        // Перевірка доступа
        self::checkAdmin();

        // Отримуємо список категорій
        $categoriesList = Category::getCategoriesListAdmin();

        // Підключаємо вид
        require_once(ROOT . '/views/admin_category/index.php');
        return true;
    }

    /**
     * Action для сторінки "Додати категорію"
     */
    public function actionCreate()
    {
        // Перевірка доступа
        self::checkAdmin();

        // Обробка форми
        if (isset($_POST['submit'])) {
            // Якщо форма відправлена
            // Отримуємо данні з форми
            $name = $_POST['name'];
            $sortOrder = $_POST['sort_order'];
            $status = $_POST['status'];

            // Прапорець помилок в формі
            $errors = false;

            // Валідуємо значення
            if (!isset($name) || empty($name)) {
                $errors[] = 'Заполните поля';
            }

            if ($errors == false) {
                // Якщо помилок немає
                // Додаємо нову категорію
                Category::createCategory($name, $sortOrder, $status);

                // Перенаправляємо користувача на сторінку керування категоріями
                header("Location: /admin/category");
            }
        }

        require_once(ROOT . '/views/admin_category/create.php');
        return true;
    }

    /**
     * Action для сторінки "Редагувати категорію"
     */
    public function actionUpdate($id)
    {
        // Перевірка доступа
        self::checkAdmin();

        // Отримуємо данні про конкретну категорію
        $category = Category::getCategoryById($id);

        // Обробка форми
        if (isset($_POST['submit'])) {
            // Якщо форма відправлена
            // Отримуємо данні з форми
            $name = $_POST['name'];
            $sortOrder = $_POST['sort_order'];
            $status = $_POST['status'];

            // Зберігаємо зміни
            Category::updateCategoryById($id, $name, $sortOrder, $status);

            // Перенаправляємо користувача на сторінку керування категоріями
            header("Location: /admin/category");
        }

        // Підключаємо вид
        require_once(ROOT . '/views/admin_category/update.php');
        return true;
    }

    /**
     * Action для сторінки "Видалити категорію"
     */
    public function actionDelete($id)
    {
        // Перевірка доступа
        self::checkAdmin();

        // Обробка форми
        if (isset($_POST['submit'])) {
            // Якщо форма відправлена
            // Видаляємо категорію
            Category::deleteCategoryById($id);

            // Перенаправляємо користувача на сторінку управління товарами
            header("Location: /admin/category");
        }

        // Підключаємо вид
        require_once(ROOT . '/views/admin_category/delete.php');
        return true;
    }

}
