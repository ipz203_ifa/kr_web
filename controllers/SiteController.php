<?php

/**
 * Контролер SiteController
 */
class SiteController
{

    /**
     * Action для головної сторінки
     */
    public function actionIndex()
    {
        // Список категорій для лівого меню
        $categories = Category::getCategoriesList();

        // Список останніх товарів
        $latestProducts = Product::getLatestProducts(6);

        // Список товарів для слайдера (рекомендовані товари)
        $sliderProducts = Product::getRecommendedProducts();

        // Підключаємо вид
        require_once(ROOT . '/views/site/index.php');
        return true;
    }

    /**
     * Action для сторінки "Контакти"
     */
    public function actionContact()
    {
        // Змінні для форми
        $userEmail = false;
        $userText = false;
        $result = false;

        // Обробка форми
        if (isset($_POST['submit'])) {
            // Якщо форма відправлена
            // Отримуємо дані з форми
            $userEmail = $_POST['userEmail'];
            $userText = $_POST['userText'];

            // Прапорець помилок
            $errors = false;

            // Валідація полів
            if (!User::checkEmail($userEmail)) {
                $errors[] = 'Неправильний email';
            }

            if ($errors == false) {
                // Якщо помилок немає
                // Надсилаємо лист адміністратору
                $adminEmail = 'kn201_bas@student.ztu.edu.ua';
                $message = "Текст: {$userText}. Від {$userEmail}";
                $subject = 'Звернення покупця';
                $result = mail($adminEmail, $subject, $message);
            }
        }

        // Підключаємо вид
        require_once(ROOT . '/views/site/contact.php');
        return true;
    }

    /**
     * Action для сторінки "Про магазин"
     */
    public function actionAbout()
    {
        // Підключаємо вид
        require_once(ROOT . '/views/site/about.php');
        return true;
    }

    /**
     * Action для сторінки "Відгуки"
     */
    public function actionReviews($page = 1)
    {
        // Список відгуків
        $listReviews = Reviews::getReviewsList($page);

        // Загальна к-ть відгуків (для посторінкової навігації)
        $total = Reviews::getTotalReviews();

        $page = preg_replace("/[^0-9]/", '', $page);

        // Створюємо об'єкт Pagination - посторінкова навігація
        $pagination = new Pagination($total, $page, Reviews::SHOW_BY_DEFAULT, 'page-');

        // Підключаємо вид
        require_once(ROOT . '/views/site/reviews.php');
        return true;
    }

    /**
     * Action для сторінки "FAQ"
     */
    public function actionFaq()
    {
        // Підключаємо вид
        require_once(ROOT . '/views/site/faq.php');
        return true;
    }

}
