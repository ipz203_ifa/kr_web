<?php

/**
 * Клас Category - модель для роботи з категоріями товарів
 */
class Category
{
    /**
     * Повертає масив категорій для списка на сайті
     * @return array <p>Масив с категоріями</p>
     */
    public static function getCategoriesList()
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Запит до БД
        $result = $db->query('SELECT id, name FROM category WHERE status = "1" ORDER BY sort_order, name ASC');

        // Отримання і повернення результатів
        $i = 0;
        $categoryList = array();
        while ($row = $result->fetch()) {
            $categoryList[$i]['id'] = $row['id'];
            $categoryList[$i]['name'] = $row['name'];
            $i++;
        }
        return $categoryList;
    }

    /**
     * Повертає масив категорій для списку в адмінпанелі <br/>
     * (при цьому в результат потрапляють і включені і вимкнені категорії)
     * @return array <p>Масив категорій</p>
     */
    public static function getCategoriesListAdmin()
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Запит до БД
        $result = $db->query('SELECT id, name, sort_order, status FROM category ORDER BY sort_order ASC');

        // Отримання і повернення результатів
        $categoryList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $categoryList[$i]['id'] = $row['id'];
            $categoryList[$i]['name'] = $row['name'];
            $categoryList[$i]['sort_order'] = $row['sort_order'];
            $categoryList[$i]['status'] = $row['status'];
            $i++;
        }
        return $categoryList;
    }

    /**
     * Видаляє категорію із заданим id
     * @param integer $id
     * @return boolean <p>Результат виконання методу</p>
     */
    public static function deleteCategoryById($id)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'DELETE FROM category WHERE id = :id';

        // Отримання та повернення результатів. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    /**
     * Редагування категорії із заданим id
     * @param integer $id <p>id категорії</p>
     * @param string $name <p>Назва</p>
     * @param integer $sortOrder <p>Порядковий номер</p>
     * @param integer $status <p>Статус <i>(включено "1", вимкнено "0")</i></p>
     * @return boolean <p>Результат виконання методу</p>
     */
    public static function updateCategoryById($id, $name, $sortOrder, $status)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = "UPDATE category
            SET 
                name = :name, 
                sort_order = :sort_order, 
                status = :status
            WHERE id = :id";

        // Отримання та повернення результатів. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':sort_order', $sortOrder, PDO::PARAM_INT);
        $result->bindParam(':status', $status, PDO::PARAM_INT);
        return $result->execute();
    }

    /**
     * Повертає категорію із зазначеним id
     * @param integer $id <p>id категорії</p>
     * @return array <p>Масив з інформацією про категорію</p>
     */
    public static function getCategoryById($id)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'SELECT * FROM category WHERE id = :id';

        // Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        // Вказуємо, що хочемо отримати дані у вигляді масиву
        $result->setFetchMode(PDO::FETCH_ASSOC);

        // Виконуємо запит
        $result->execute();

        // Повертаємо дані
        return $result->fetch();
    }

    /**
     * Повертає текстове пояснення статусу для категорії :<br/>
     * <i>0 - Прихована, 1 - Відображається</i>
     * @param integer $status <p>Статус</p>
     * @return string <p>Текстове пояснення</p>
     */
    public static function getStatusText($status)
    {
        switch ($status) {
            case '1':
                return 'Відображається';
                break;
            case '0':
                return 'Прихована';
                break;
        }
    }

    /**
     * Додає нову категорію
     * @param string $name <p>Назва</p>
     * @param integer $sortOrder <p>Порядковий номер</p>
     * @param integer $status <p>Статус <i>(включено "1", вимкнено "0")</i></p>
     * @return boolean <p>Результат додавання запису до таблиці</p>
     */
    public static function createCategory($name, $sortOrder, $status)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'INSERT INTO category (name, sort_order, status) '
                . 'VALUES (:name, :sort_order, :status)';

        // Отримання та повернення результатів. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':sort_order', $sortOrder, PDO::PARAM_INT);
        $result->bindParam(':status', $status, PDO::PARAM_INT);
        return $result->execute();
    }

}
