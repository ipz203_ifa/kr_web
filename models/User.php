<?php

/**
 * Класс User - модель для роботи з користувачами
 */
class User
{
    /**
     * Реєстрація користувача
     * @param string $name <p>Ім'я</p>
     * @param string $email <p>E-mail</p>
     * @param string $password <p>Пароль</p>
     * @return boolean <p>Результат виконання методу</p>
     */
    public static function register($name, $email, $password)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        //Хешуємо пароль
        $password = password_hash($password, PASSWORD_DEFAULT);

        // Текст запиту до БД
        $sql = 'INSERT INTO user (name, email, password) '
                . 'VALUES (:name, :email, :password)';

        // Отримання та повернення результатів. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        return $result->execute();
    }

    /**
     * Редагування даних користувача
     * @param integer $id <p>id користувача</p>
     * @param string $name <p>Ім'я</p>
     * @param string $password <p>Пароль</p>
     * @return boolean <p>Результат виконання методу</p>
     */
    public static function edit($id, $name, $password)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        //Хешуємо пароль
        $password = password_hash($password, PASSWORD_DEFAULT);

        // Текст запиту до БД
        $sql = "UPDATE user 
            SET name = :name, password = :password 
            WHERE id = :id";

        // Отримання та повернення результатів. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        return $result->execute();
    }

    /**
     * Додавання нового відгука користувачем
     * @param string $user_name <p>Ім'я користувача</p>
     * @param string $text <p>Текст відгука</p>
     * @param string $date <p>Дата</p>
     * @return boolean <p>Результат виконання метода</p>
     */
    public static function add($user_name, $text, $date)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'INSERT INTO reviews '
            . '(user_name, text, date)'
            . 'VALUES '
            . '(:user_name, :text, :date)';

        // Отримання та повернення результатів. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':user_name', $user_name, PDO::PARAM_INT);
        $result->bindParam(':text', $text, PDO::PARAM_STR);
        $result->bindParam(':date', $date, PDO::PARAM_STR);
        return $result->execute();
    }

    /**
     * Перевіряємо, чи існує користувач із заданими $email і $password
     * @param string $email <p>E-mail</p>
     * @param string $password <p>Пароль</p>
     * @return mixed : integer user id or false
     */
    public static function checkUserData($email, $password)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'SELECT * FROM user WHERE email = :email';

        // Отримання та повернення результатів. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_INT);
        $result->execute();

        // Звертаємося до запису
        $user = $result->fetch();

        //Перевірка паролю
        $pass = password_verify($password, $user['password']);

        if ($user && $pass) {
            // Якщо запис існує, повертаємо id користувача
            return $user['id'];
        }
        return false;
    }

    /**
     * Запам'ятовуємо користувача
     * @param integer $userId <p>id користувача</p>
     */
    public static function auth($userId)
    {
        // Записуємо ідентифікатор користувача у сесію
        $_SESSION['user'] = $userId;
    }

    /**
     * Повертає ідентифікатор користувача, якщо він авторизований.<br/>
     * Інакше перенаправляє на сторінку входу
     * @return string <p>Ідентифікатор користувача</p>
     */
    public static function checkLogged()
    {
        // Якщо сесія є, повернімо ідентифікатор користувача
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }

       header("Location: /user/login");
    }

    /**
     * Перевіряє чи є користувач гостем
     * @return boolean <p>Результат виконання методу</p>
     */
    public static function isGuest()
    {
        if (isset($_SESSION['user'])) {
            return false;
        }
        return true;
    }

    /**
     * Перевіряє ім'я: не менше, ніж 2 символи
     * @param string $name <p>Ім'я</p>
     * @return boolean <p>Результат виконання методу</p>
     */
    public static function checkName($name)
    {
        if (strlen($name) >= 2) {
            return true;
        }
        return false;
    }

    /**
     * Перевіряє телефон: не менше ніж 10 символів та український номер
     * @param string $phone <p>Телефон</p>
     * @return boolean <p>Результат виконання методу</p>
     */
    public static function checkPhone($phone)
    {
        $pattern = "/^\+380\d{3}\d{2}\d{2}\d{2}$/";
        if (strlen($phone) >= 10 && preg_match($pattern, $phone)) {
            return true;
        }
        return false;
    }

    /**
     * Перевіряє ім'я: не менше ніж 6 символів
     * @param string $password <p>Пароль</p>
     * @return boolean <p>Результат виконання методу</p>
     */
    public static function checkPassword($password)
    {
        if (strlen($password) >= 6) {
            return true;
        }
        return false;
    }

    /**
     * Перевіряє email
     * @param string $email <p>E-mail</p>
     * @return boolean <p>Результат виконання методу</p>
     */
    public static function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    /**
     * Перевіряє, чи не зайнятий email іншим користувачем
     * @param type $email <p>E-mail</p>
     * @return boolean <p>Результат виконання методу</p>
     */
    public static function checkEmailExists($email)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'SELECT COUNT(*) FROM user WHERE email = :email';

        // Отримання та повернення результатів. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();

        if ($result->fetchColumn())
            return true;
        return false;
    }

    /**
     * Повертає користувача із зазначеним id
     * @param integer $id <p>id користувача</p>
     * @return array <p>Масив з інформацією про користувача</p>
     */
    public static function getUserById($id)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'SELECT * FROM user WHERE id = :id';

        // Отримання та повернення результатів. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        // Вказуємо, що хочемо отримати дані у вигляді масиву
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $result->execute();

        return $result->fetch();
    }

    /**
     * Повертає список користувачів
     * @return array <p>Масив з інформацією про користувачів</p>
     */
    public static function getUsersList()
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Отримання та повернення результатів
        $result = $db->query('SELECT id, name, email, role FROM user ORDER BY id ASC');
        $usersList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $usersList[$i]['id'] = $row['id'];
            $usersList[$i]['name'] = $row['name'];
            $usersList[$i]['email'] = $row['email'];
            $usersList[$i]['role'] = $row['role'];
            $i++;
        }
        return $usersList;
    }

    /**
     * Редагування даних користувача
     * @param integer $id <p>id користувача</p>
     * @param string $name <p>Ім'я</p>
     * @param string $email <p>Email</p>
     * @param string $role <p>Роль</p>
     * @return boolean <p>Результат виконання методу</p>
     */
    public static function updateUserById($id, $name, $email, $role)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = "UPDATE user 
            SET name = :name, email = :email, role = :role 
            WHERE id = :id";

        // Отримання та повернення результатів. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':role', $role, PDO::PARAM_STR);

        return $result->execute();
    }

    /**
     * Видаляє користувача із заданим id
     * @param integer $id <p>id користувача</p>
     * @return boolean <p>Результат виконання методу</p>
     */
    public static function deleteUserById($id)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'DELETE FROM user WHERE id = :id';

        // Отримання та повернення результатів. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }
}
