<?php

/**
 * Класс Product - модель для роботи з товарами
 */
class Product
{

    // Кількість товарів, що відображаються за замовчуванням
    const SHOW_BY_DEFAULT = 6;

    /**
     * Повертає масив останніх товарів
     * @param type $count [optional] <p>Кількість</p>
     * @param type $page [optional] <p>Номер поточної сторінки</p>
     * @return array <p>Масив з товарами</p>
     */
    public static function getLatestProducts($count = self::SHOW_BY_DEFAULT)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'SELECT id, name, price, is_new, availability FROM product '
                . 'WHERE status = "1" ORDER BY id DESC '
                . 'LIMIT :count';

        // Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':count', $count, PDO::PARAM_INT);

        // Вказуємо, що хочемо отримати дані у вигляді масиву
        $result->setFetchMode(PDO::FETCH_ASSOC);
        
        // Виконання команди
        $result->execute();

        // Отримання та повернення результатів
        $i = 0;
        $productsList = array();
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['is_new'] = $row['is_new'];
            $productsList[$i]['availability'] = $row['availability'];
            $i++;
        }
        return $productsList;
    }

    /**
     * Повертає масив шуканих товарів
     * @param type $count [optional] <p>Кількість</p>
     * @param type $page [optional] <p>Номер поточної сторінки</p>
     * @return array <p>Масив з товарами</p>
     */
    public static function getSearchProducts($count = self::SHOW_BY_DEFAULT, $search)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = "SELECT id, name, price, is_new, availability FROM product "
            . "WHERE status = '1' AND name LIKE '%$search%' ORDER BY id DESC "
            . "LIMIT :count";

        // Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':count', $count, PDO::PARAM_INT);

        // Вказуємо, що хочемо отримати дані у вигляді масиву
        $result->setFetchMode(PDO::FETCH_ASSOC);

        // Виконання команди
        $result->execute();

        // Отримання та повернення результатів
        $i = 0;
        $productsList = array();
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['is_new'] = $row['is_new'];
            $productsList[$i]['availability'] = $row['availability'];
            $i++;
        }
        return $productsList;
    }

    /**
     * Повертає список товарів у зазначеній категорії
     * @param type $categoryId <p>id категорії</p>
     * @param type $page [optional] <p>Номер сторінки</p>
     * @return type <p>Масив з товарами</p>
     */
    public static function getProductsListByCategory($categoryId, $page = 1, $availability, $order, $param, $par = 0)
    {
        $limit = Product::SHOW_BY_DEFAULT;
        // Зміщення (для запита)
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        if($order == 1 && $param == 1)
            $sql = 'SELECT id, name, price, is_new, availability FROM product '
                    . 'WHERE status = 1 AND category_id = :category_id '
                    . 'ORDER BY price ASC LIMIT :limit OFFSET :offset';
        else if($order == 1 && $param == 2)
            $sql = 'SELECT id, name, price, is_new, availability FROM product '
                . 'WHERE status = 1 AND category_id = :category_id '
                . 'ORDER BY price DESC LIMIT :limit OFFSET :offset';
        else if($order == 2 && $param == 1)
            $sql = 'SELECT id, name, price, is_new, availability FROM product '
                . 'WHERE status = 1 AND category_id = :category_id '
                . 'ORDER BY name DESC LIMIT :limit OFFSET :offset';
        else if($order == 2 && $param == 2)
            $sql = 'SELECT id, name, price, is_new, availability FROM product '
                . 'WHERE status = 1 AND category_id = :category_id '
                . 'ORDER BY name DESC LIMIT :limit OFFSET :offset';
        else
            $sql = 'SELECT id, name, price, is_new, availability FROM product '
                . 'WHERE status = 1 AND category_id = :category_id '
                . 'ORDER BY id ASC LIMIT :limit OFFSET :offset';

        // Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':category_id', $categoryId, PDO::PARAM_INT);
        $result->bindParam(':limit', $limit, PDO::PARAM_INT);
        $result->bindParam(':offset', $offset, PDO::PARAM_INT);

        // Виконання команди
        $result->execute();

        // Отримання та повернення результатів
        $i = 0;
        $products = array();
        while ($row = $result->fetch()) {
            if(in_array($row['availability'], $availability)) {
                $products[$i]['id'] = $row['id'];
                $products[$i]['name'] = $row['name'];
                $products[$i]['price'] = $row['price'];
                $products[$i]['is_new'] = $row['is_new'];
                $products[$i]['availability'] = $row['availability'];
                $i++;
            }
        }
        if($par === 1) {
            return $result->rowCount();
        }
        else
            return $products;
    }

    /**
     * Повертає продукт із зазначеним id
     * @param integer $id <p>id товару</p>
     * @return array <p>Масив з інформацією про товар</p>
     */
    public static function getProductById($id)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'SELECT * FROM product WHERE id = :id';

        // Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);

        // Вказуємо, що хочемо отримати дані у вигляді масиву
        $result->setFetchMode(PDO::FETCH_ASSOC);

        // Виконання команди
        $result->execute();

        // Отримання та повернення результатів
        return $result->fetch();
    }

    /**
     * Повертаємо кількість товарів у зазначеній категорії
     * @param integer $categoryId
     * @return integer
     */
    public static function getTotalProductsInCategory($categoryId)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'SELECT count(id) AS count FROM product WHERE status="1" AND category_id = :category_id';

        // Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':category_id', $categoryId, PDO::PARAM_INT);

        // Виконання команди
        $result->execute();

        // Повертаємо значення count - кількість
        $row = $result->fetch();
        return $row['count'];
    }

    /**
     * Повертає список товарів із зазначеними індентифікаторами
     * @param array $idsArray <p>Масив з ідентифікаторами</p>
     * @return array <p>Масив зі списком товарів</p>
     */
    public static function getProdustsByIds($idsArray)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Перетворюємо масив на рядок для формування умови у запиті
        $idsString = implode(',', $idsArray);

        // Текст запиту до БД
        $sql = "SELECT * FROM product WHERE status='1' AND id IN ($idsString)";

        $result = $db->query($sql);

        // Вказуємо, що хочемо отримати дані у вигляді масиву
        $result->setFetchMode(PDO::FETCH_ASSOC);

        // Отримання та повернення результатів
        $i = 0;
        $products = array();
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['code'] = $row['code'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            $products[$i]['availability'] = $row['availability'];
            $i++;
        }
        return $products;
    }

    /**
     * Повертає список популярних товарів
     * @return array <p>Масив з товарами</p>
     */
    public static function getRecommendedProducts()
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Отримання та повернення результатів
        $result = $db->query('SELECT id, name, price, is_new, availability FROM product '
                . 'WHERE status = "1" AND is_recommended = "1" '
                . 'ORDER BY id DESC');
        $i = 0;
        $productsList = array();
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['is_new'] = $row['is_new'];
            $productsList[$i]['availability'] = $row['availability'];
            $i++;
        }
        return $productsList;
    }

    /**
     * Повертає список товарів
     * @return array <p>Масив з товарами</p>
     */
    public static function getProductsList()
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Отримання та повернення результатів
        $result = $db->query('SELECT id, name, price, code, availability FROM product ORDER BY id ASC');
        $productsList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['code'] = $row['code'];
            $productsList[$i]['price'] = $row['price'];
            $productsList[$i]['availability'] = $row['availability'];
            $i++;
        }
        return $productsList;
    }

    /**
     * Видаляє товар із зазначеним id
     * @param integer $id <p>id товару</p>
     * @return boolean <p>Результат виконання методу</p>
     */
    public static function deleteProductById($id)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'DELETE FROM product WHERE id = :id';

        // Отримання та повернення результатів. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    /**
     * Редагує товар із заданим id
     * @param integer $id <p>id товару</p>
     * @param array $options <p>Масив з інформацією про товар</p>
     * @return boolean <p>Результат виконання методу</p>
     */
    public static function updateProductById($id, $options)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = "UPDATE product
            SET 
                name = :name, 
                code = :code, 
                price = :price, 
                category_id = :category_id, 
                brand = :brand, 
                availability = :availability, 
                description = :description, 
                is_new = :is_new, 
                is_recommended = :is_recommended, 
                status = :status
            WHERE id = :id";

        // Отримання та повернення результатів. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        return $result->execute();
    }

    /**
     * Додає новий товар
     * @param array $options <p>Масив з інформацією про товар</p>
     * @return integer <p>id доданої до таблиці запису</p>
     */
    public static function createProduct($options)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'INSERT INTO product '
                . '(name, code, price, category_id, brand, availability,'
                . 'description, is_new, is_recommended, status)'
                . 'VALUES '
                . '(:name, :code, :price, :category_id, :brand, :availability,'
                . ':description, :is_new, :is_recommended, :status)';

        // Отримання та повернення результатів. Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':code', $options['code'], PDO::PARAM_STR);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':brand', $options['brand'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':description', $options['description'], PDO::PARAM_STR);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':is_recommended', $options['is_recommended'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        if ($result->execute()) {
            // Якщо запит виконано успішно, повертаємо id доданого запису
            return $db->lastInsertId();
        }
        // Інакше повертаємо 0
        return 0;
    }

    /**
     * Повертає текстове пояснення наявності товару:<br/>
     * <i>0 - Товар відсутній, 1 - В наявності</i>
     * @param integer $availability <p>Статус</p>
     * @return string <p>Текстове пояснення</p>
     */
    public static function getAvailabilityText($availability)
    {
        switch ($availability) {
            case '1':
                return 'В наявності';
                break;
            case '0':
                return 'Товар відсутній';
                break;
        }
    }

    /**
     * Повертає шлях до зображення
     * @param integer $id
     * @return string <p>Шлях до зображення</p>
     */
    public static function getImage($id)
    {
        // Назва зображення-пустушки
        $noImage = 'no-image.jpg';

        // Шлях до папки з товарами
        $path = '/upload/images/products/';

        // Шлях до зображення товару
        $pathToProductImage = $path . $id . '.jpg';

        if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToProductImage)) {
            // Якщо зображення для товару існує
            // Повертаємо шлях зображення товару
            return $pathToProductImage;
        }

        // Повертаємо шлях зображення-пустушки
        return $path . $noImage;
    }

}
