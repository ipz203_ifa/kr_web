<?php

/**
 * Класс Cabinet - модель для роботи з кабінетом
 */
class Cabinet
{
    /**
     * Повертає список покупок
     * @return array <p>Масив з замовленнями</p>
     */
    public static function getOrdersList($user_id)
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Отримання та повернення результатів
        $sql = 'SELECT id, user_name, user_phone, user_comment, user_id, date, products, status FROM product_order WHERE user_id = :user_id ORDER BY id ASC';
        // Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);

        // Виконання команди
        $result->execute();

        // Отримання та повернення результатів
        $i = 0;
        $ordersList = array();
        while ($row = $result->fetch()) {
            $ordersList[$i]['id'] = $row['id'];
            $ordersList[$i]['user_name'] = $row['user_name'];
            $ordersList[$i]['user_phone'] = $row['user_phone'];
            $ordersList[$i]['user_comment'] = $row['user_comment'];
            $ordersList[$i]['user_id'] = $row['user_id'];
            $ordersList[$i]['date'] = $row['date'];
            $ordersList[$i]['products'] = $row['products'];
            $ordersList[$i]['status'] = self::getStatusText($row['status']);
            $i++;
        }
        return $ordersList;
    }

    public static function getProductsInHistory($products, $param)
    {
        $products = json_decode($products, true);
        $productsName = array_keys($products);
        $productsCount = array();
        $productsNames = array();
        if($param==1){
            foreach ($productsName as $product){
                array_push($productsNames, Product::getProductById($product));
            }
            return $productsNames;
        }
        else{
            foreach ($products as $product){
                array_push($productsCount, $product);
            }
            return $productsCount;
        }
    }

    /**
     * Повертає текстове пояснення статусу замовлення:<br/>
     * <i>1 - Нове замовлення, 2 - В обробці, 3 - Доставляється, 4 - Закрито</i>
     * @param integer $status <p>Статус</p>
     * @return string <p>Текстове пояснення</p>
     */
    public static function getStatusText($status)
    {
        switch ($status) {
            case '1':
                return 'Нове замовлення';
                break;
            case '2':
                return 'В обробці';
                break;
            case '3':
                return 'Доставляється';
                break;
            case '4':
                return 'Закрито';
                break;
        }
    }


}