<?php

/**
 * Клас Reviews - модель для роботи з відгуками
 */
class Reviews
{
    // Кількість відгуків, що відображаються за замовчуванням
    const SHOW_BY_DEFAULT = 2;

    /**
     * Повертає список відкугів
     * @return array <p>Масив з відгуками</p>
     */
    public static function getReviewsList($page = 1)
    {
        $limit = Reviews::SHOW_BY_DEFAULT;
        // Зміщення (для запита)
        $page = preg_replace("/[^0-9]/", '', $page);
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;

        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'SELECT id, user_name, text, date FROM reviews ORDER BY date DESC LIMIT :limit OFFSET :offset';

        // Використовується підготовлений запит
        $result = $db->prepare($sql);
        $result->bindParam(':limit', $limit, PDO::PARAM_INT);
        $result->bindParam(':offset', $offset, PDO::PARAM_INT);

        // Виконання команди
        $result->execute();

        // Отримання і повернення результатів
        $i = 0;
        $reviewsList = array();
        while ($row = $result->fetch()) {
            $reviewsList[$i]['id'] = $row['id'];
            $reviewsList[$i]['user_name'] = $row['user_name'];
            $reviewsList[$i]['text'] = $row['text'];
            $reviewsList[$i]['date'] = $row['date'];
            $i++;
        }

        return $reviewsList;
    }

    /**
     * Повертаємо загальну кількість відгуків
     * @return integer
     */
    public static function getTotalReviews()
    {
        // З'єднання з БД
        $db = Db::getConnection();

        // Текст запиту до БД
        $sql = 'SELECT count(id) AS count FROM reviews';

        // Використовується підготовлений запит
        $result = $db->prepare($sql);

        // Виконання команди
        $result->execute();

        // Повертаємо значення count - кількість
        $row = $result->fetch();
        return $row['count'];
    }
}

